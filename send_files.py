#!/usr/bin/env python
# Copyright (c) 2018 European Spallation Source ERIC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import click
import shlex
import subprocess
from pathlib import Path
from pprint import pprint
from epicsarchiver import ArchiverAppliance
from process_archives import get_files_per_archiver


@click.command()
@click.option("--verbose", "-v", is_flag=True, help="Increase verbosity")
@click.argument("filenames", nargs=-1, type=click.Path(exists=True))
def cli(filenames, verbose):
    """Send PVs to the archiver(s) from the given files"""
    if not filenames:
        raise click.UsageError("You must pass the files to send in arguments")
    archivers = get_files_per_archiver([Path(name) for name in filenames])
    if verbose:
        pprint(archivers)
    for hostname, files in archivers.items():
        if files:
            click.echo(
                f"Sending PVs from {','.join([str(f) for f in files])} to {hostname}"
            )
            archiver = ArchiverAppliance(hostname)
            result = archiver.archive_pvs_from_files(files, appliance=archiver.identity)
            if verbose:
                pprint(result)
        else:
            click.echo(f"No new PV for {hostname}")


if __name__ == "__main__":
    cli()
